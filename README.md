# Weather Finder App

On this project I used:

* [ReactJS] - A JavaScript library for building user interfaces
* [date-fns] - Modern JavaScript date utility library.
* [Bootstrap] - Build responsive, mobile-first projects on the web with the world's most popular front-end component library.

   [ReactJS]: <https://github.com/facebook/react/>
   [date-fns]: <https://github.com/date-fns/date-fns/>
   [Bootstrap]: <https://getbootstrap.com/>

To run it just type:

```sh
yarn && yarn start
```

I tried to separete everything, to make it easy to understand.

Upcoming features should be:

* Change the background according to the weather
* Autocomplete Country and City
