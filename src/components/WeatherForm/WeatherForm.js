import React from "react";
import './WeatherForm.css'

import { Button, Form, FormGroup, Input } from "reactstrap";

const WeatherForm = props => (
  <div className="weather-form">
  <Form onSubmit={props.getWeather}>
    <FormGroup>
      <Input className="input-field" bsSize="lg" type="text" name="city" placeholder="Type the City..." />
    </FormGroup>
    <FormGroup>
      <Input className="input-field" bsSize="lg" type="text" name="country" placeholder="Type the Country..." />
    </FormGroup>
    <Button size="lg" className="buttom-full">Get the Weather</Button>
  </Form>
  </div>
);

export default WeatherForm;
