import React from "react";
import format from 'date-fns/format';
import './WeatherDisplay.css';

const hourFormat = 'HH:mm';

const WeatherDisplay = props => (
  <div className="weather-display">
    {props.city &&
      props.country && (
        <p>
          Location: &nbsp;
          <span>
            {props.city}, {props.country}
          </span>
        </p>
      )}
    {props.temperature && (
      <p>
        Temperature: &nbsp;
        <span>{props.temperature}º</span>
      </p>
    )}
    {props.humidity && (
      <p>
        Humidity: &nbsp;
        <span>{props.humidity}% </span>
      </p>
    )}
    {props.description && (
      <p>
        Conditions: &nbsp;
        <span>{props.description}</span>
      </p>
    )}
    {props.sunrise && (
      <p>
        Sunrise:&nbsp;
          <span>
            {format(props.sunrise * 1000, hourFormat)} UTC
          </span> 
      </p>
    )}
    {props.sunset && (
      <p>
        Sunset: &nbsp;
        <span>
            {format(props.sunset * 1000, hourFormat)} UTC
          </span> 
      </p>
    )}
    {props.error && <p className="api-error">{props.error}</p>}
  </div>
);

export default WeatherDisplay;
