import React from "react";

import WeatherForm from "../WeatherForm/WeatherForm";
import WeatherDisplay from "../WeatherDisplay/WeatherDisplay";
import Notice from "../Notice/Notice";

const API_KEY = "ded638580d1370332a2566c9bf99ab60";

class Weather extends React.Component {
  state = {
    temperature: undefined,
    city: undefined,
    country: undefined,
    humidity: undefined,
    description: undefined,
    sunrise: undefined,
    sunset: undefined,
    error: undefined,
    didSubmit: false
  };
  getWeather = async e => {
    e.preventDefault();
    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;
    const api_call = await fetch(
      `https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metric`
    );
    const data = await api_call.json();
    const valid = Number(data.cod);

    if (city && country && valid === 200) {
      this.setState({
        temperature: data.main.temp,
        city: data.name,
        country: data.sys.country,
        humidity: data.main.humidity,
        description: data.weather[0].main,
        sunrise: data.sys.sunrise,
        sunset: data.sys.sunset,
        error: "",
        didSubmit: true
      });
    } else {
      this.setState({
        temperature: undefined,
        city: undefined,
        country: undefined,
        humidity: undefined,
        description: undefined,
        sunrise: undefined,
        sunset: undefined,
        error: "Please enter some correct values."
      });
    }
  };

  render() {
    const didSubmit = this.state.didSubmit;

    if (didSubmit) {
      return (
        <div>
          <WeatherForm getWeather={this.getWeather} />

          <WeatherDisplay
            temperature={this.state.temperature}
            humidity={this.state.humidity}
            city={this.state.city}
            country={this.state.country}
            description={this.state.description}
            sunrise={this.state.sunrise}
            sunset={this.state.sunset}
            error={this.state.error}
          />
        </div>
      );
    } else {
      return (
        <div>
          <WeatherForm getWeather={this.getWeather} />
          <Notice />
        </div>
      );
    }
  }
}

export default Weather;
