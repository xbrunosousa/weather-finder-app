import React from "react";
import "./Notice.css";

const Notice = () => {
  return (
    <div className="notice-display">
      <p>
        Hi! Thanks for using me! If you can't find your city, make sure to put it in English, it may help.
      </p>
    </div>
  );
};

export default Notice;
