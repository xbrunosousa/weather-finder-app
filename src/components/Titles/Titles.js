import React from "react";
import "./Titles.css";

const Titles = () => (
  <div className="weather-title">
    <h3>How is the Weather Today?</h3>
    <div className="p-holder">
      <p>
        Who made me is not a designer, but he likes to do some stuff with React,
        just to show that he knows how to consume APIs and blablabla.
      </p>
      <p>
        If you see some bug or something that you don't like, could you please
        let me know on the{" "}
        <a href="https://github.com/pedrofilho20/weather-finder-app">
          GitHub Project
        </a>?
      </p>
      <p>
        He used the OpenWeatherMap API to take this info, Bootstrap to make it
        responsible and date-fns to make the sunrise and sunset display
        properly. For this gradient background I used the Vice City set from
        UIGradients.
      </p>
    </div>
  </div>
);

export default Titles;
