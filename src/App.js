import React from "react";

import Titles from "./components/Titles/Titles";
import Weather from "./components/Weather/Weather";

import { Container, Row, Col } from "reactstrap";

class App extends React.Component {
  render() {
    return (
      <div>
        <div className="wrapper">
          <div className="weather-box">
            <Container>
              <Row>
                <Col lg="5" sm="12" xs="12">
                  <Titles />
                </Col>
                <Col lg="7" sm="12" xs="12">
                  <Weather />
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
